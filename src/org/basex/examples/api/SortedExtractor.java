package org.basex.examples.api;

import java.io.*;
import java.nio.file.*;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** 
 *  main settings is in the bundle file
 *  
 *  also needed files referred in fileInSource/fileInWholeSource and listSources
 *  main queries with pattern file names are in ..\sortedExtractorFiles\ folder
 *  
 *  these file are sources to receive file with query for every folder listed in listSources.txt
 *  for every path in listSources.txt (listSources in the bundle) it looks for file AllProperties.xml (fileName in the bundle)
 *  and make query on it to receive same tags but sorted
 *  
 *  Query should have path to file inside so template query (fileInSource/fileInWholeSource) is used where 
 *  &Variable is substituted with right file name (fileInWhole).
 *  
 *  From every file we receive two files - first with all Field and Variable tags ordered (fileOut)
 *  second with only trigger actions (fileOutTrig).
 *  These files are placed to the source directory from listSources
 *  
 *  
 * Documentation for queries http://docs.basex.org/wiki/Clients
 *
 * @author Sergii Kotov Epam
 */
public final class SortedExtractor {
  /**
   */
  static SourceFile sf;
  	
  public static void main(final String... args) throws IOException {
	  sf= new SourceFile();
	  String sLine="";
	  String sOutput="";
	  final long time = System.nanoTime();
	  while (sLine!=null) {
		  //to next xml file
		  sLine=sf.nextSource();
		  // get output name
		  if (sLine!=null) {
			  execQuery(true,sLine);
			  execQuery(false,sLine);
		  }
	  } 
	  final double ms = (System.nanoTime() - time) / 1000000.0d;
      System.out.println("\n" + ms + " ms");
	}
  
  	private static void execQuery (boolean isTrg, String sLine) throws IOException {
  	  String sOutput=sf.getCurrentRes(isTrg);
	  System.out.println(sOutput);
	  // prepare query. get query with current file name
	  // create query file
	  sf.prepareFile(sLine,isTrg);
	  try(BaseXClient session = new BaseXClient(sf.host, sf.port, sf.login, sf.pass)) {
		  //					    query file name		  output file name
		  session.execute("RUN "+sf.getCurrentSource(),sf.createOutput(sOutput));
	  }
  	}
  }

class SourceFile 
{
	private ResourceBundle rb;
	private BufferedReader brSrc;
	private String currentSource;
	String login;
	String pass;
	String host;
	int port;
	static String pathQrFiles=Paths.get("").toAbsolutePath().toString()+"\\sortedExtractorFiles\\"; 
	
	public SourceFile () throws FileNotFoundException {
	 rb = ResourceBundle.getBundle("org.basex.examples.api.bundleBaseX");
	 brSrc = new BufferedReader(new FileReader(pathQrFiles+rb.getString("listSources")));
	 login=rb.getString("login");
	 pass=rb.getString("pass");
	 host=rb.getString("host");
	 port=Integer.parseInt(rb.getString("port"));
	}
	
	public String getCurrentSource() {
		return pathQrFiles+rb.getString("fileInWhole");		
	}

	public String getCurrentRes(boolean isForTrig) {
		return currentSource+(isForTrig?rb.getString("fileOutTrig"):rb.getString("fileOut"));	
	}
	
	public String nextSource() {
	 try {
		 currentSource=brSrc.readLine();
		 return (currentSource==null)?null:currentSource+rb.getString("fileName");
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void prepareFile (String srcFl,boolean isForTrig) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(pathQrFiles+
					(isForTrig?rb.getString("fileInSource"):rb.getString("fileInWholeSource"))));
	        StringBuilder sb = new StringBuilder();
	        String line;
			line = br.readLine();
	        String patternString1 = "(&.*?)\\W";
	        Pattern pattern = Pattern.compile(patternString1);
	        Matcher matcher = pattern.matcher(line);
	        matcher.find();
	        //System.out.println(matcher.group(1));
	        line=line.replace(matcher.group(1),"'"+srcFl+"'" );
	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        File file = new File(getCurrentSource());
	        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
	            writer.write(sb.toString());
	        }
	        br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public FileOutputStream createOutput (String pthTo) throws IOException {
	      File file;
	      file = new File(pthTo);
	      
	      Path p =Paths.get(pthTo);
	      if (Files.exists(p)) {
	    	  Files.delete(p);
	      }
		  return new FileOutputStream(file);
	}
}


