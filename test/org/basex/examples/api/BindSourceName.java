package org.basex.examples.api;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.junit.Test;

public class BindSourceName {
	@Test
	public void test() throws IOException {
		 ResourceBundle rb = ResourceBundle.getBundle("org.basex.examples.api.bundleBaseX");
		 BufferedReader brSrc = new BufferedReader(new FileReader(rb.getString("listSources")));
		 BufferedReader br = new BufferedReader(new FileReader(rb.getString("fileInWholeSource")));
		 String srcFl=brSrc.readLine();
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
		        String patternString1 = "(&.*?)\\W";
		        Pattern pattern = Pattern.compile(patternString1);
		        Matcher matcher = pattern.matcher(line);
		        matcher.find();
		        //System.out.println(matcher.group(1));
		        line=line.replace(matcher.group(1),"'"+srcFl+"'" );
		        //line = matcher.replaceFirst("'"+srcFl+"'");
		        while (line != null) {
		            sb.append(line);
		            sb.append("\n");
		            line = br.readLine();
		        }
		        //System.out.println(sb.toString());
		        File file = new File(rb.getString("fileInWhole"));
		        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
		            writer.write(sb.toString());
		        }
		    } finally {
		        br.close();
		    }	
		    assertTrue(1==1);
	}

}
