package org.basex.examples.api;

import static org.junit.Assert.*;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.junit.Test;

public class bundle0 {

	@Test
	public void test() {
		ResourceBundle rb = ResourceBundle.getBundle("org.basex.examples.api.bundleBaseX");
		/*
		Enumeration <String> keys = rb.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			String value = rb.getString(key);
			System.out.println(key + ": " + value);
		}
		*/
		assertTrue(rb.keySet().size()==6);
	}

}
